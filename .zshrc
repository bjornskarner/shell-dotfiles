PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting

EDITOR="emacsclient -c" # default editor emacs.
VISUAL="emacsclient -c" # as above

# to enable color themes in terminal.
TERM=xterm-256color

# no more vi!
alias vim='emacsclient -c'
alias vi='emacsclient -c'

# tmuxinator setings.
[[ -s $HOME/.tmuxinator/scripts/tmuxinator ]] && source $HOME/.tmuxinator/scripts/tmuxinator

# This provides a more extensive tab completion.
autoload -U compinit
compinit

# Enables tab completion from both ends.
setopt completeinword

# Completion will be case insensitive.
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'

# Better completion for killall
zstyle ':completion:*:killall:*' command 'ps -u $USER -o cmd'

# Colors for ls
if [[ -x "`whence -p dircolors`" ]]; then
  eval `dircolors`
  alias ls='ls -F --color=auto'
else
  alias ls='ls -F'
fi

# Alias for the most commonly used ls.
alias ll='ls -l'
alias la='ls -a'
alias lal='ls -al'

# superglobs
setopt extendedglob
unsetopt caseglob

# Bigger history file.
HISTFILE=~/.zhistory
HISTSIZE=SAVEHIST=10000
setopt incappendhistory 
setopt sharehistory
setopt extendedhistory

# Could be used to put command in history.
setopt interactivecomments # pound sign in interactive prompt

# Use .. instead of cd.
setopt auto_cd

# A nicer prompt.
PS1='[%T] %n:%~:> '

# Reports CPU usage for processes taking longer than 10 seconds to execute.
REPORTTIME=10

# Aptitude aliases.
alias 'a=sudo apt-get'
alias 'ai=sudo apt-get install'
alias 'ar=sudo apt-get remove'
alias 'au=sudo apt-get update'
alias 'ag=sudo apt-get safe-upgrade'
alias 'as=apt-cache search'
alias 'aw=apt-cache show'
alias 'aptlp=apt-list-packages'

# Git aliases
alias 'gc=git commit'
alias 'gcm=git commit -m'
alias 'ga=git add -A'
alias 'gacm=ga; gcm'
alias 'gstat=git status'

# Lists all packages by size descending in case we want to see the bigger packages.
function apt-list-packages {
  dpkg-query -W --showformat='${Installed-Size} ${Package} ${Status}\n' | grep -v deinstall | sort -n | awk '{print $1" "$2}'
}

# Alias for Spec No Rails, only to be used in Rails projects.
alias 'spn=rspec ./spec_no_rails/'

# Function used to print pretty json using pythons Json.tool. 
function pjson {
    if [ $# -gt 0 ];
    then
        for arg in $@
        do
            if [ -f $arg ];
            then
                less $arg | python -m json.tool
            else
                echo "$arg" | python -m json.tool
            fi
        done
    fi
}
